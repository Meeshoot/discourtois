# DISCOURTOIS

Discourtois is a discord bot that censors any message with a forbidden word via a blacklist. 
The bot sends the message back to the user, telling them which word is problematic. 

The bot also posts the message in a dedicated channel so that the moderators are warned.
This bot allows, via a CLI, to display the forbidden words, add and remove words from the blacklist

## Installation

Go to the install directory of your choice then 
```
git clone https://gitlab.com/afterthinking/discourtois
```
Make sure the user:group permissions are good. 

move to the directory of the project :
```
cd discourtois
```

Create a virtual environement if necessary : 
```
python -m venv ./venv
```
activate it :
```
source venv/bin/activate
```

Install the requirements listed in the file *requirements.txt* : 
```
pip install -r requirements.txt
```

Create a file `.env` and add the following informations : 
```
TOKEN=<BOT_TOKEN>
CHANNEL_LOG=<CHANNEL_LOG>
LOG_DIR=<PATH_TO_LOG>
BLACKLIST=<PATH_TO_BLACKLIST>
```
Replace the <VARIABLES> with the correct data : 
- TOKEN : your bot TOKEN provided by the discord API 
- CHANNEL_LOG : the channel where the bot will display logs for the admins (bot is running and any deleted message)
- LOG_DIR : path to the log directory for the bot (server side) (has to end with the '/') 
- BLACKLIST : path to the blacklist file

Create the log directory : 
```
mkdir log
```

Create the blacklist file :
```
touch blacklist
```

Everything is configured ! you can now run your bot : 
```
python src/main.py & 
```


## Usage

```python
# print all forbidden words 
!blacklist-display 

# add words to the blacklist
!blacklist-add word1 word2... wordn

# remove words from the blacklist
!blacklist-rm word1 word2... wordn
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPL V3](https://www.gnu.org/licenses/gpl-3.0.en.html)
