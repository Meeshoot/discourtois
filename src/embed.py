# libraries
import discord

import log



def public_swear_censor(pseudo):
    embed = discord.Embed() 

    embed.title = "Attention " + pseudo + " ! Utilisation de mots interdit !"
    embed.description = "L'After Thinking est un lieu où un français châtié est requis."

    embed.color = int('0xFF0000', 16)

    embed.set_thumbnail(url="https://c.tenor.com/GtP-sMA0eVoAAAAC/tenor.gif")

    return embed


def private_swear_censor(title, sentence):
    embed = discord.Embed() 

    embed.title = title 
    embed.description = sentence

    embed.color = int('0xFF0000', 16)

    embed.set_thumbnail(url="https://c.tenor.com/GtP-sMA0eVoAAAAC/tenor.gif")

    return embed



def simple_embed(title, sentence):
    embed = discord.Embed() 

    embed.title = title
    embed.description = sentence

    embed.color = int('0x0000FF', 16)

    # embed.set_thumbnail(url="")

    return embed



