# libraries
import datetime
import os
import re

import log
import embed

from dotenv import load_dotenv

load_dotenv()

blacklist_file=str(os.getenv("BLACKLIST"))
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))



blacklist=[]


"""
"   Load the words from blacklist file.
"   @args : none 
"   @return : nothing
"""
def load_blacklist():
    global blacklist

    file = open(blacklist_file, 'r')
    for line in file.readlines():
        blacklist.append(line.rstrip())

    file.close()



"""
"   Save the blacklist into a file
"   @args : none
"   @return : nothing
"""
def save_blacklist():
	file = open(blacklist_file, 'w')
	file.write('')
	for line in blacklist:
		 file.write(line + '\n')

	file.close() #close file


"""
"   Display the list of the words in the blacklist
"   @arg1 : object - discord object to use the discord API
"   @return : nothing
"""
async def blacklist_display(bot):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    await admin_channel.send("Voici la liste des mots interdit sur le serveur :")
    await admin_channel.send(blacklist)
 

"""
"   Add a word or multiple words into the blacklist
"   @arg1 : object - discord object to use the discord API
"   @arg3 : object - message object from the discord API
"   @return : nothing
"""
async def blacklist_add(bot, message):
    admin_channel = bot.get_channel(CHANNEL_LOG)

    new_words = message.split()
    new_words.pop(0)
    if(new_words): 
        for word in new_words:
            if word not in blacklist:
                blacklist.append(word)
                await admin_channel.send("Le mot « " + word + " » est désormais censuré sur le serveur")
            else:
                await admin_channel.send("Le mot « " + word + " » est déjà dans la liste noire.")
    else:
        await admin_channel.send("Une demande d'ajout de mot à la liste noire a été faite mais aucun mot n'a été spécifié.")

    save_blacklist()



"""
"   Remove a word or multiple words into the blacklist
"   @arg1 : object - discord object to use the discord API
"   @arg2 : object - message object from the discord API
"   @return : nothing
"""
async def blacklist_rm(bot, message):
    admin_channel = bot.get_channel(CHANNEL_LOG)

    new_words = message.split()
    new_words.pop(0)
    if(new_words): 
        for word in new_words:
            if word in blacklist:
                blacklist.remove(word)
                await admin_channel.send("Le mot « " + word + " » n'est plus censuré sur le serveur")
            else:
                await admin_channel.send("Le mot « " + word + " » n'était pas censuré.")
    else:
        await admin_channel.send("Une demande de suppression de mot à la liste noire a été faite mais aucun mot n'a été spécifié.")

    save_blacklist()


async def blacklist_help(bot):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    await admin_channel.send("Voici les commandes pour modérateurs :\n\n```* blacklist-add mot1 mot2 [...] motN\t\t: ajoute de 1 à N mot à la liste de censure\n* blacklist-rm mot1 mot2 [...] motN \t\t: retire de 1 à N mot à la liste de censure\n* blacklist-display \t\t\t\t\t\t: affiche la liste des mots censurés\n* blacklist-help\t\t\t\t\t\t\t: affiche ce message```")


"""
"   Check if message has any word in the blacklist and censor it.
"   @arg1 : object - discord object to use the discord API
"   @arg3 : object - message object from the discord API
"   @return : nothing
"""
async def check_swearword(bot, message):

    # handle messages sent by moderators to add admin tools in CLI style
    if "🦊Modération" in [role.name for role in message.author.roles] or message.author.guild_permissions.administrator:
        if message.content == "blacklist-display":
            await blacklist_display(bot)
            return 
        elif message.content.startswith("blacklist-add"):
            await blacklist_add(bot, message.content)
            return
        elif message.content.startswith("blacklist-rm"):
            await blacklist_rm(bot, message.content)
            return 
        elif message.content.startswith("blacklist-help"): 
            await blacklist_help(bot)
            return

    # check if words of the blacklist is found in the message content
    sentence = message.content.lower()
    chars = ['\.', '\,', '\?', '\;', '\:', '\/', '\§', '\!', '\'', '\"']
    sentence = re.sub("|".join(chars), "", sentence)
    forbidden_word_found = [word for word in blacklist if word in sentence.split()]


    if forbidden_word_found:
        log.log("(on_message) - message with a forbidden word : " + message.content)

        origin_channel = message.channel
        admin_channel = bot.get_channel(CHANNEL_LOG)
        dm_channel = await message.author.create_dm()

        # censor and warn publicly the user
        await message.delete()
        await origin_channel.send(embed = embed.public_swear_censor(message.author.name))

        # put words found into a « printable » string
        words_str=""
        for word in forbidden_word_found:
            words_str = words_str + " « " + word + " »,"
        words_str = words_str[:-1] # remove the last coma

        # send deleted message to both user & admins
        # adapt sentence if one or multiple words
        title = "Attention " + message.author.name + " !"
        warning_sentence = "L'emploi d'un français châtié est requis sur ce serveur.\nIl est interdit d'utiliser "
        if len(forbidden_word_found) == 1:
            warning_sentence += "le mot"
        else:
            warning_sentence += "les mots" 
        warning_sentence += words_str + ".\nVoici ton message :"

        await dm_channel.send(embed = embed.private_swear_censor(title, warning_sentence))
        await dm_channel.send(message.content)
        await admin_channel.send(embed = embed.private_swear_censor(title, warning_sentence))
        await admin_channel.send(message.content)
